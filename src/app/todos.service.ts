import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from '@angular/common/http';
import { catchError, delay, Observable, throwError } from "rxjs";

export interface Todo {
  title: string;
  completed: boolean;
  id?: number | string;
}

@Injectable({providedIn: 'root'})
export class TodosService {

  constructor(private http: HttpClient) {}

  addTodo(todo: Todo): Observable<Todo> {
    return this.http.post<Todo>('https://jsonplaceholder.typicode.com/todos', todo);
  }

  fetchTodos(): Observable<Todo[]> {
    return this.http.get<Todo[]>('https://jsonplaceholder.typicode.com/todos', {
      params: new HttpParams().set('_limit', 3)
    })
    .pipe(delay(500))
  }

  removeTodo(id: number | string): Observable<void> {
    return this.http.delete<void>(`https://jsonplaceholder.typicode.com/todos/${id}`)
  }

  compeleteTodo(id: number | string): Observable<Todo> {
    return this.http.put<Todo>(`https://jsonplaceholder.typicode.com/todos/${id}`, {
      completed: true
    })
    .pipe(catchError(error => {
      console.log(error.message);
      throw `Error on server. Details: ${error.message}`
      
    }))
  }

}