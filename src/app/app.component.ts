import { Component, OnInit } from '@angular/core';

import { Todo, TodosService } from './todos.service';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  todos: Todo[] = [];
  todoTitle = '';
  loading = false;
  error = '';

  constructor(private todosService: TodosService) {}

  ngOnInit(): void {
    this.fetchTodos();
  }

  addTodo() {
    if (!this.todoTitle.trim()) {
      return
    }
    this.todosService.addTodo({
      title: this.todoTitle,
      completed: false
    }).subscribe(todo => {
      this.todos.push(todo)
      this.todoTitle = '';
    })
  }

  removeTodo(id: number| string| undefined) {
    if (id) {
      this.todosService.removeTodo(id)
      .subscribe(() => {
        this.todos = this.todos.filter(todo => +todo.id! !== +id)
      })
    }
  }

  fetchTodos() {
    this.loading = true;
    this.todosService.fetchTodos()
    .subscribe({
      next: todos => {
          this.todos = todos;
          this.loading = false;
        },
      error: error => this.error = error.message,
    })
  }

  compeleteTodo(id: number | string | undefined) {
    if (id) {
      this.todosService.compeleteTodo(id)
      .subscribe({
        next: todo => {
          const modifiedTodo = this.todos.find(t => t.id === todo.id);
          if (modifiedTodo) {
            modifiedTodo.completed = true;
          }
        },
        error: err => console.log(err)
      })
    }
  }

}   

